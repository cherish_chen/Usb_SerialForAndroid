package chan.com.usbservice;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.hoho.android.usbserial.datas.UserService;
import com.hoho.android.usbserial.datas.notify.NotificationListener;
import com.hoho.android.usbserial.examples.R;

import java.util.Date;

/**
 * Created by chen on 2017/12/28.
 */
public class DemoActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);
        final TextView tv = findViewById(R.id.tv_main);
        UserService userService = new UserService(this, null, "XXXXX");
        userService.registerNotificationLisener(new NotificationListener() {
            @Override
            public void onNotify(final Object noti) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv.setText(noti.toString() + new Date().getTime());
                    }
                });
            }
        });
    }
}
