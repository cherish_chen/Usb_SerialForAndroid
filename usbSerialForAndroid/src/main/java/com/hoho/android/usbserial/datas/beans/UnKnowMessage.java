package com.hoho.android.usbserial.datas.beans;

import com.hoho.android.usbserial.datas.BaseMessage;

/**
 * Created by chen on 2017/12/29.
 */
public class UnKnowMessage extends BaseMessage {

    private String mEcontent;

    public String getmEcontent() {
        return mEcontent;
    }

    public void setmEcontent(String mEcontent) {
        this.mEcontent = mEcontent;
    }
}
